use logos::Logos;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Logos)]
pub enum Token {
    #[token("crate")]
    Crate,
    #[token("use")]
    Use,
    #[token("let")]
    Let,
    #[token("mut")]
    Mut,
    #[token("pub")]
    Pub,
    #[token("fn")]
    Fn,
    #[token("extern")]
    Extern,
    #[token("struct")]
    Struct,
    #[token("enum")]
    Enum,
    #[token("type")]
    Type,
    #[token("async")]
    Async,
    #[token("await")]
    Await,
    #[token("loop")]
    Loop,
    #[token("for")]
    For,
    #[token("while")]
    While,
    #[token("continue")]
    Continue,
    #[token("break")]
    Break,
    #[token("if")]
    If,
    #[token("else")]
    Else,
    #[token("match")]
    Match,
    #[token("return")]
    Return,
    #[token("yield")]
    Yield,
    #[regex(r"(0[box])?\d+", priority=3)]
    IntegerLiteral,
    #[regex(r"(?:0[box])?(?:\d+\.?\d*|\d*\.?\d+)(?:e\d+)?", priority=2)]
    FloatLiteral,
    #[regex(r"(?:0[box])?(?:\d+\.?\d*|\d*\.?\d+)(?:e\d+)?j")]
    ImaginaryLiteral,
    #[regex(r"'.'|'\\''")]
    CharLiteral,
    #[regex(r##""(?:[^"\\]|\\.)*""##)]
    StrLiteral,
    #[regex(r"\w+")]
    Identifier,
    #[regex(r"///[^\n]*\n")]
    DocLine,
    #[regex(r"//[^\n]*\n")]
    LineComment,
    #[regex(r"/\*/*\*/")]
    BlockComment,
    #[regex(r"\s+", logos::skip)]
    WhiteSpace,
    #[token("::")]
    PathSep,
    #[token("==")]
    Eq,
    #[token("!=")]
    Neq,
    #[token("<=")]
    Leq,
    #[token(">=")]
    Geq,
    #[token("<")]
    Lt,
    #[token(">")]
    Gt,
    #[token("&&")]
    And,
    #[token("||")]
    Or,
    #[token("=>")]
    BigArrow,
    #[token("->")]
    SmallArrown,
    #[token("++")]
    Increment,
    #[token("--")]
    Decrement,
    #[token("=")]
    Assign,
    #[token("+=")]
    AddAssign,
    #[token("-=")]
    SubAssign,
    #[token("*=")]
    MulAssign,
    #[token("/=")]
    DivAssign,
    #[token("%=")]
    RemAssign,
    #[token("&=")]
    BitAndAssign,
    #[token("|=")]
    BitOrAssign,
    #[token("^=")]
    BitXorAssign,
    #[token(">>=")]
    ShiftRightAssign,
    #[token("<<=")]
    ShiftLeftAssign,
    #[token("!")]
    Complement,
    #[token("?")]
    Question,
    #[token("#")]
    Sharp,
    #[token("@")]
    At,
    #[token("+")]
    Add,
    #[token("-")]
    Sub,
    #[token("*")]
    Mul,
    #[token("/")]
    Div,
    #[token("%")]
    Rem,
    #[token("&")]
    BitAnd,
    #[token("|")]
    BitOr,
    #[token("^")]
    BitXor,
    #[token(">>")]
    ShiftRight,
    #[token("<<")]
    ShiftLeft,
    #[token(".")]
    Dot,
    #[token(",")]
    Coma,
    #[token(";")]
    Semi,
    #[token(":")]
    Colon,
    #[token("..")]
    Range,
    #[token("..=")]
    InclusiveRange,
    #[token("'")]
    Appostrophe,
    #[token("(")]
    LeftPar,
    #[token(")")]
    RightPar,
    #[token("{")]
    LeftAngled,
    #[token("}")]
    RightAngled,
    #[token("[")]
    LeftBracket,
    #[token("]")]
    RightBracket,
    #[error]
    Unknown,
}

#[test]
fn lex() {
    let text = r#"
/// pi
use std::f64::π;
let 2jπ = 2j * π;
let ω = 2.4e9 * 2jπ;
let a = 'a'; // hey
let _ = '_test;
    "#;
    println!("CODE:\n{}\n\nTOKENS:\n", text);
    let mut lexer = Token::lexer(text);
    while let Some(token) = lexer.next() {
        println!("{:?}: {}", token, lexer.slice());
    }
}
